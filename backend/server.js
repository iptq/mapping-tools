const fs = require("fs");
const path = require("path");
const express = require("express");
const app = express();
const mongo = require('mongodb').MongoClient;
const md5 = require("md5-file");
const cronjob = require('cron').CronJob;
require('dotenv').config()

app.use(express.static(path.join(__dirname, 'data')));
app.use(express.static(path.join(__dirname, '../frontend/build')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, "../frontend/build/index.html"));
});

new cronjob("* */10 * * * *", () => {
  const manifest = { paths: [] };
  walkDir(path.join(__dirname, "/data/updater/resources"), (fPath, isFile) => {
    manifest.paths.push({
      path: fPath,
      hash: isFile ? md5.sync(fPath) : "",
      isFile: isFile
    });
  });

  const obj = JSON.stringify(manifest);
  fs.writeFileSync(path.join(__dirname, "/data/updater/manifest.json"), obj);
  console.log("Generated Manifest");
}, null, true);


app.post("/download/win/installer", (req, res) => {
  res.setHeader("content-type", "octet/binary");
  fs.createReadStream(path.join(__dirname, "data/current/mapping-tools.exe")).pipe(res);
});

app.post("/download/win/archive", (req, res) => {
  res.setHeader("content-type", "octet/binary");
  fs.createReadStream(path.join(__dirname, "data/current/mapping-tools.zip")).pipe(res);
});

app.post("/faq/faqs", (req, res) => {
  mongo.connect(process.env.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true }, async (err, client) => {
    if (err) throw err;

    try {
      const db = client.db("mappingtools");
      const collection = db.collection("faq");
      const cursor = collection.find();
      const data = await cursor.map(item => {
        if (item === null) {
          client.close();
          return;
        }
        else return item;
      }).toArray();

      data.sort((a, b) => {
        return b.relevancy - a.relevancy;
      });

      res.send(data);
    } catch (e) {
      res.status(501);
      res.end();
      throw e;
    }
  });
});

app.post("/changelog/logs", (req, res) => {
  mongo.connect(process.env.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true }, async (err, client) => {
    if (err) {
      throw err;
    }

    try {
      const db = client.db("mappingtools");

      const collection = db.collection('changelogs');
      const cursor = collection.find();
      const data = await cursor.map(item => {
        if (item == null) {
          client.close();
          return;
        }
        return item;
      }).toArray();

      data.sort((a, b) => {
        if (a.date < b.date) return 1;
        else return -1;
      });

      res.send(data);
    } catch (e) {
      res.status(501);
      res.end();
      throw e;
    }
  });
});

function walkDir(dirPath, callback) {
  fs.readdirSync(dirPath).forEach(function (fName) {
    var fPath = path.join(dirPath, fName);
    var fStat = fs.statSync(fPath);
    if (fStat.isFile()) {
      callback(fPath, true);
    } else if (fStat.isDirectory()) {
      callback(fPath, false);
      walkDir(fPath, callback);
    }
  });
}

app.listen(process.env.PORT);